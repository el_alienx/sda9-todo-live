export default function CreateItem(title) {
  // Constants
  const { title, completed } = item;

  // Create HTML tags
  const li = document.createElement("li");
  const checkbox = document.createElement("input");
  const span = document.createElement("span");

  // Setup HTML tags
  li.classList.add("task-item");
  checkbox.setAttribute("type", "checkbox");
  checkbox.setAttribute("checked", completed);
  span.innerHTML = title;

  // Append HTML tags
  li.appendChild(checkbox);
  li.appendChild(span);

  return li;
}
